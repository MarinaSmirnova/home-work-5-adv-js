const usersUrl = "https://ajax.test-danit.com/api/json/users";
const postsUrl = "https://ajax.test-danit.com/api/json/posts";

const container = document.getElementById("container");

Promise.all([
    fetch(usersUrl),
    fetch(postsUrl)
])
    .then(([usersData, postsData]) => {
        return Promise.all([usersData.json(), postsData.json()]);
    })

    .then(([users, posts]) => {
        const cardList = document.createElement("ul");
        cardList.classList.add("card__list");
        container.appendChild(cardList);

        for (const post of posts) {
            const card = createCard(post, users);
            card.show(cardList);
        }
    })

    .catch(e => {console.log("Error: " + e);});


function createCard(post, usersArr) {
    const user = usersArr.find(user => user.id == post.userId);

    ({title, body: text, userId: id} = post);
    ({name, email} = user);

    return new Card(name, email, id, title, text);
}


class Card {
    constructor(name, email, id, title, text) {
        this.name = name;
        this.email = email;
        this.id = id;
        this.title = title;
        this.text = text;
    }

    show(container) {
        const cardItem = document.createElement("li");
        cardItem.classList.add("card__item");
        cardItem.innerHTML = `
        <div class="card__header">
            <div>
                <span class="card__name">${this.name}</span>
                <a class="card__email" href="mailto:${this.email}">${this.email}</a>
            </div>
            <div class="card__delete"></div>
        </div>
        <h3 class="card__title">${this.title}</h3>
        <p class="card__text">${this.text}</p>
         `;

         const deleteBtn = cardItem.querySelector(".card__delete");
         deleteBtn.addEventListener("click", () => this.deleteCard(cardItem));

         container.appendChild(cardItem);
    }

    deleteCard(cardItem) {
        console.log("Del");

        fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, {
            method: "DELETE"
        })

        .then(response => {
            if (response.ok) {
                cardItem.remove();
            } else {
                console.log("Помилка при видаленні картки:");
            }
        })
        .catch(e => {
            console.log("Error: " + e);
        });




    }

    



}
